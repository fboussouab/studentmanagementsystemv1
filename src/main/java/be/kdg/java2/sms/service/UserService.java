package be.kdg.java2.sms.service;

import be.kdg.java2.sms.database.UserDAO;
import be.kdg.java2.sms.exceptions.StudentException;

import java.util.logging.Logger;

public class UserService {
    private static final Logger L = Logger.getLogger(UserService.class.getName());
    private static final int MIN_PASSWORD_LENGTH = 3;//todo: change to 10!
    private UserDAO userDAO;

    public UserService() {
        userDAO = new UserDAO();
    }

    public void addUser(String name, String password) throws StudentException {
        L.info("Trying to add user:" + name);
        if (password==null||password.length()<MIN_PASSWORD_LENGTH) {
            L.info("Password not strong enough");
            throw new StudentException("Password not strong enough");
        }
        if (userDAO.getUserByName(name)!=null) {
            L.info("Username already in use!");
            throw new StudentException("Username already in use!");
        }
        userDAO.addUser(new User(name, password));
    }

    public boolean login(String username, String password) throws StudentException {
        L.info("Trying to login user " + username);
        User user = userDAO.getUserByName(username);
        if (user == null) {
            return false;
        } else {
            return user.getPassword() != null && user.getPassword().equals(password);
        }
    }
}
