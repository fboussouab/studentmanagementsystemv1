package be.kdg.java2.sms.view;

import be.kdg.java2.sms.exceptions.StudentException;
import be.kdg.java2.sms.service.Student;
import be.kdg.java2.sms.service.StudentsService;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class StudentsPresenter {
    private static final Logger L = Logger.getLogger(StudentsPresenter.class.getName());
    private StudentsView studentsView;
    private StudentsService studentsService;

    public StudentsPresenter(StudentsView studentsView, StudentsService studentsService) {
        this.studentsView = studentsView;
        this.studentsService = studentsService;
        loadStudents();
        studentsView.getBtnSave().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Student student = new Student(studentsView.getTfName().getText(),
                        studentsView.getDpBirthday().getValue(),Double.parseDouble(studentsView.getTfLength().getText()));
                studentsService.addStudent(student);
                loadStudents();
            }
        });
    }

    private void loadStudents() {
        L.info("Loading list of students");
        try {
            List<Student> myList = studentsService.getAllStudents();
            studentsView.getTvStudents().setItems(FXCollections.observableList(myList));
        } catch (StudentException studentException) {
            L.warning("Unable to load students: " + studentException);
            new Alert(Alert.AlertType.ERROR, "Unable to load students:\n" + studentException.getMessage()).showAndWait();
        }
    }

}
