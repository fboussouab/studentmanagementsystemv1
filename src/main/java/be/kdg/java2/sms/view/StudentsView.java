package be.kdg.java2.sms.view;

import be.kdg.java2.sms.service.Student;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.time.LocalDate;
import java.util.logging.Logger;

public class StudentsView extends BorderPane {
    private static final Logger L = Logger.getLogger(StudentsView.class.getName());
    private TableView tvStudents;//no generics, would lead us to far...
    private TextField tfName;
    private TextField tfLength;
    private DatePicker dpBirthday;
    private Button btnSave;

    @SuppressWarnings("unchecked")
    public StudentsView() {
        L.info("Construction StudentsView...");
        tvStudents = new TableView<>();
        tfName = new TextField();
        tfName.setPromptText("Name");
        tfLength = new TextField();
        tfLength.setPromptText("Length");
        dpBirthday = new DatePicker();
        dpBirthday.setPromptText("Birthday");
        btnSave = new Button("Save");
        super.setCenter(tvStudents);
        BorderPane.setMargin(tvStudents, new Insets(10));
        TableColumn<String, Student> column1 = new TableColumn<>("Name");
        column1.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn<LocalDate, Student> column2 = new TableColumn<>("Birthday");
        column2.setCellValueFactory(new PropertyValueFactory<>("birthday"));
        TableColumn<String, Student> column3 = new TableColumn<>("Length");
        column3.setCellValueFactory(new PropertyValueFactory<>("length"));
        tvStudents.getColumns().addAll(column1, column2, column3);
        HBox hbBottom = new HBox(tfName, dpBirthday, tfLength, btnSave);
        hbBottom.setSpacing(10);
        super.setBottom(hbBottom);
        BorderPane.setMargin(hbBottom, new Insets(10));
    }

    TableView<Student> getTvStudents() {
        return tvStudents;
    }

    TextField getTfName() {
        return tfName;
    }

    TextField getTfLength() {
        return tfLength;
    }

    Button getBtnSave() {
        return btnSave;
    }

    DatePicker getDpBirthday() {
        return dpBirthday;
    }
}
